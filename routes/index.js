var mongoose = require('mongoose');
var Account = require('../models/account');
var express = require('express');
var passport = require('passport');
var User = require('../models/account');
var router = express.Router();

exports.register = function (req, res) {
    console.log("registering: " + req.body.username);
    User.register(new User({
        username: req.body.username,
        firstname: req.body.firstname
    }), req.body.password, function (err, user) {
        if (err) {
            console.log(err);
            return res.send(err);
        } else {
            user.game.points += 10;
            user.game.eventText.push("10 points for registering, welcome!");
            user.save(function (err) {
                if (err) {
                    console.log("Error when registering new user");
                }
                else {
                    console.log("New user registered: " + user._id);
                }
            });

            res.send({
                success: true,
                user: user
            });
        }
    });
};

exports.login = function (req, res, next) {
    User.authenticate()(req.body.username, req.body.password, function (err, user, options) {
        if (err) return next(err);
        if (user === false) {
            res.send({
                message: options.message,
                success: false
            });
        } else {
            req.login(user, function (err) {
                var utc = new Date().toJSON().slice(0,10);

                //Giving points if user has not signed in today.
                if(user.game.eventText.indexOf("1 point for signing in " + utc) < 0) {
                    user.game.points += 1;
                    user.game.eventText.push("1 point for signing in " + utc);
                    user.save(function (err) {
                        if (err) {
                            console.log("Error when giving points for signing in");
                        }
                        else {
                            console.log("Sign in: " + user._id);
                        }
                    });
                }
                res.send({
                    success: true,
                    user: user
                });
            });
        }
    });
};

exports.getUpdatedStats = function (req, res) {
    if (req.user) {
        Account.findById(req.user._id, function (err, updatedUser) {
            if (err) {
                res.send({
                    success: false,
                    message: 'Could not find user'
                });
            }
            else {
                console.log("Stats som returneres: " + updatedUser.game.points);
                res.send({
                    success: true,
                    user: updatedUser
                });
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Could not find user'
        });
    }
}


exports.getLogin = function (req, res) {
    if (req.user) {
        return res.send({
            success: true,
            user: req.user
        });

    } //res.send(500, {status:500, message: 'internal error', type:'internal'}); == deprecated
    else{
        res.send({
            success: false,
            message: 'Not logged in'
        });
    }
};

exports.logout = function (req, res) {
    if (req.user) {
        req.logout();
        console.log("logged out");
        return res.send({
            success: true,
            message: 'Logout successful'
        });
    }
    else{
        console.log("No user to log out");
        return res.send({
            success: false,
            message: 'No user to log out'
        });
    }
};