var express = require('express');
var passport = require('passport');
var request = require('request'); //Used for making http requests to external dbs
var Account = require('../models/account');
var Publication = require('../models/publication');
var FlagEvent = require('../models/flagEvent');
var Version = require('../models/version');

//Set up for gameification
var pointsForAddingPublication = 10;
var pointsForVerifiedPublication = 40;
var pointsForVerifying = 10;
var pointsForRejectingPublication = 10;
var pointsForVoting = 2;
var pointsForMetadata = 2;
var pointsForVersion = 25;

exports.checkLink = function (){

    var missingLink = 0;
    var errorHeader = 0;
    var toBeDeleted = 0;
    var checked = 0;
    var checkedPrintCounter = 0;


    console.log("running ")

    Publication.count({}, function(err, count){
        console.log( "Number of publications: ", count );


        var stream = Publication.find().stream();

        stream.on('data', function (doc) {
            this.pause()

            checked ++;
            checkedPrintCounter ++;
            if((Math.floor(checkedPrintCounter / count * 100) >= 1)){
                checkedPrintCounter = 0;
                console.log(Math.floor((checked / count * 100)) + "% complete");
                console.log( missingLink + " deleted so far because of missing link")
                console.log("--------------")

            }


            if(doc.versions[0].link != ''){
                /* var testURL = doc.versions[0].link;
                 request(testURL, function (error, response, body) {
                 if (error) {
                 }
                 else {
                 if (response.statusCode > 399 && response.statusCode < 500)
                 errorHeader ++;
                 toBeDeleted ++;
                 console.log("400s: " + errorHeader)
                 console.log("To be deleted: " + toBeDeleted);
                 console.log(response.statusCode);
                 }
                 })
                 */


                this.resume()
            }
            else{

                Publication.findByIdAndRemove(doc._id, function (err, success) {
                    if (err) {
                        console.log("error removing empty string");
                    }
                    else {
                        missingLink ++;
                    }
                })
                /*
                 missingLink ++;
                 console.log("Missing link: " + missingLink);
                 toBeDeleted ++;
                 console.log("To be deleted: " + toBeDeleted);
                 */
                this.resume()
            }

        })

        stream.on('error', function (err) {
            console.log("error: " + err)
            // handle err
        })

        stream.on('close', function () {
            console.log("all done")
            // all done
        })
    });
}


exports.getPublicationById = function (req, res) {
    Publication.findById(req.params._id, function (err, publication){
        if (err)
            res.send({
                success: false,
                message: 'Publication not found',
            });
        if(publication){
            res.send({
                success: true,
                message: 'Publication found',
                publication: publication
            });
        }
    });
};


exports.citationCount = function (req,res){
    var doi = req.body.doi;
    doi = doi.replace('http://dx.doi.org/', '');

    var options = { method: 'GET',
        url: 'http://api.elsevier.com/content/search/scopus',
        qs:
        { query: 'DOI(' + doi + ')',
            field: 'citedby-count' },
        headers:
        { 'postman-token': '9e30fa93-0142-ceb5-4400-3bcecc103a10',
            'cache-control': 'no-cache',
            'x-els-apikey': '04ddf748c808d2ac52b8c9e71733ba7e' } };

    request(options, function (error, response, body) {
        res.send(body);
    });
}

exports.search = function (req, res) {

    var field = req.params.queryType;
    var queryString = req.params.query;

    //Title is only field system is capable of doing full-text search in, as a limitation of mongoDB.
    if(field == 'publication_title'){
        Publication.find(
            {
                $text : { $search : queryString }
            },
            { score : { $meta: "textScore" } }
        ).limit(20)
            .sort({ score : { $meta : 'textScore' } })
            .exec(function(err, results) {
                if (err) {
                    console.log("Error: " +  err);
                }
                res.json(results);
            })
    }
    else if(field == 'doi'){
        console.log("QUERY: " + queryString);
        Publication.find({'doi': queryString}).limit(20).exec(function(err, results) {
            if (err) {
                console.log("Error: " +  err);
            }
            res.json(results);
        })
    }
    // db.publications.find({$and[{"publication_title": {"$type": 2, "$regex": /\p{Latin}/}}, {publication_title: {"$regex": /.*ç õ.*/}}]}, {publication_title: 1}).pretty()
    else{
        var query = {};
        query[field] = new RegExp('.*' +queryString+'.*', "i");

        Publication.find(query).limit(25).exec(function(err, results) {
            if (err) {
                console.log("Error: " +  err);
            }
            res.json(results);

        })
    }
};

exports.createPublication = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined){
            username = req.user.facebook.name
        }
        if ((req.body.title != undefined) && (req.body.link != undefined)) {

            //When creating a new publication, a version document based on the link provided is also created
            var newVersion = new Version({
                link: req.body.link,
                type: req.body.type,
                user: username
            })

            var newPublication = new Publication({
                verified: false,
                publication_title: req.body.title,
                abstract: req.body.abstract,
                authors: req.body.authors,
                doi: req.body.doi,
                publisher: req.body.publisher,
                year: req.body.year,
                pages: req.body.pages,
                journal_title: req.body.journal_title,
                journal_number: "",
                journal_volume: "",
                ISSN: req.body.ISSN,
                eISSN: req.body.eISSN,


                addedById: req.user._id
            });

            newPublication.versions.push(newVersion);//Version inserted into publication prior to saving publication.
            newPublication.save(function (err, newPublication) {
                if (err) {
                    console.log(err);
                    res.send(err);
                }
                else {
                    console.log(newPublication._id);
                    Account.findById(req.user._id, function (err, user) {
                        if (err) {
                            res.send({
                                success: false,
                                message: 'Could not find user'
                            });
                        }
                        else {

                            user.game.points += pointsForAddingPublication;
                            user.game.pubAdded++;
                            user.game.eventText.push(pointsForAddingPublication + " points for adding a paper: " + req.body.title);
                            console.log(username + " has " + user.game.points + " points");

                            user.save(function (err) {
                                if (err) {
                                    res.send({
                                        success: false,
                                        message: 'Could not update user profile with points'
                                    });
                                }
                                else {
                                    console.log(username + " has " + user.game.points + " points");
                                    return res.send({
                                        success: true,
                                        _id: newPublication._id,
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        else {
            res.send({
                success: false,
                message: 'A link and the title is required'
            });
        }
    }
    else{
        res.send({
            success: false,
            message: 'not authorized'
        });
    }
}

exports.createVersion = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined){
            username = req.user.facebook.name
        }
        if ((req.body.type != undefined) && (req.body.link != undefined) && (req.body._id != undefined)) {
            Publication.findById(req.body._id, function (err, doc){
                if (err){
                    res.send ("couldn't find the title to be added a version to");
                }
                else {
                    var newVersion = new Version({
                        link: req.body.link,
                        type: req.body.type,
                        user: username
                    });
                    doc.versions.push(newVersion);

                    doc.save(function (err) {
                        if (err) {
                            res.send({
                                success: false,
                                message: 'Unable to save version.'
                            });
                        }
                        else {
                            Account.findById(req.user._id, function (err, user) {
                                if (err) {
                                    res.send({
                                        success: false,
                                        message: 'Could not find user'
                                    });
                                }
                                else {

                                    user.game.points += pointsForVersion;
                                    user.game.versionAdded++;
                                    user.game.eventText.push(pointsForVersion + " points for adding a version to the paper: " + doc.publication_title);
                                    user.save(function (err) {
                                        if (err) {
                                            console.log("Error when giving points for adding version");
                                        }
                                    });
                                }
                            });
                            console.log("New version added by" + username);
                            res.send({
                                success: true,
                                message: 'New version created!',
                            });
                        }

                    });
                }
            });
        }
        else{
            res.send({
                success: false,
                message: 'Missing fields'
            });
        }
    }
    else {
        res.send({
            success: false,
            message: 'Sorry, you need to be rank ' + addVersionRequirement + " to do this."
        });
    }
};

exports.voteOnVersion = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined){
            username = req.user.facebook.name
        }
        if ((req.body.type != undefined) && (req.body.publication_id != undefined) && (req.body.version_id != undefined)) {
            Publication.findById(req.body.publication_id, function (err, doc) {
                if (err) {
                    res.send({
                        success: false,
                        message: 'Invalid publication'
                    });
                }
                else {
                    if(((doc.versions.id(req.body.version_id).upvoting_users.indexOf(username)) > -1) && (req.body.type == "upvote")){
                        res.send({
                            success: false,
                            message: "You have already voted this up."
                        });
                    }
                    else if(((doc.versions.id(req.body.version_id).downvoting_users.indexOf(username)) > -1) && (req.body.type == "downvote")){
                        res.send({
                            success: false,
                            message: "You have already voted this down."
                        });
                    }
                    //Checking i
                    else{

                        //Checks to see if user has already voted on this version(either up or down). If so, he should not be awarded any more points.
                        if(((doc.versions.id(req.body.version_id).upvoting_users.indexOf(username)) == -1) && ((doc.versions.id(req.body.version_id).downvoting_users.indexOf(username)) == -1)){
                            Account.findById(req.user._id, function (err, user) {
                                if (err) {
                                    console.log("Could not find user to give points for voting on version");
                                }
                                else {

                                    user.game.points += pointsForVoting;
                                    user.game.pubVoted++;
                                    user.game.eventText.push(pointsForVoting + " points for voting on a version of paper: " + doc.publication_title);
                                    user.save(function (err) {
                                        if (err) {
                                            console.log("Could not give points for voting");
                                        }
                                        else {
                                            console.log("Points given to user for voting");
                                        }
                                    });
                                }
                            });
                        }

                        //Removing potential previous votes and registers new vote.
                        if (req.body.type == "upvote"){
                            doc.versions.id(req.body.version_id).downvoting_users = doc.versions.id(req.body.version_id).downvoting_users.filter(function (i) {
                                return i != username;
                            });
                            doc.versions.id(req.body.version_id).upvoting_users.push(username);
                        }
                        else if (req.body.type == "downvote") {
                            doc.versions.id(req.body.version_id).upvoting_users = doc.versions.id(req.body.version_id).upvoting_users.filter(function (i) {
                                return i != username;
                            });
                            doc.versions.id(req.body.version_id).downvoting_users.push(username);
                        }
                        doc.save(function (err) {
                            if (err) {
                                res.send({
                                    success: false,
                                    message: 'Could not save vote'
                                });
                            }
                            else {
                                console.log("-->" + doc.versions.id(req.body.version_id).upvoting_users.indexOf(username) + " <-->" + doc.versions.id(req.body.version_id).downvoting_users.indexOf(username));

                                res.send({
                                    success: true,
                                    message: 'Vote registered!'
                                });
                            }
                        });
                    }
                }
            });
        }
        else {
            res.send({
                success: false,
                message: 'Could not save vote; missing fields.'
            });
        }
    }
    else {
        res.send({
            success: false,
            message: 'Please log in to vote.'
        });
    }
};

exports.getVerificationList = function (req, res) {
    if (req.user) {
        Publication.find({'verified': 'false'}, function (err, docs) {
            if (err) {
                res.send({
                    success: false,
                    message: 'No publications are waiting for verification'
                });
            }
            if (docs) {
                res.json(docs);
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.getFlagList = function (req, res) {
    if (req.user) {
        Publication.find({'flagged': 'true'}, function (err, docs) {
            if (err) {
                res.send({
                    success: false,
                    message: 'No publications are flagged'
                });
            }
            if (docs) {
                res.send({
                    success: true,
                    publications: docs
                });
            }
            else {
                res.send({
                    success: false,
                    message: 'unable to retrieved flagged publications'
                })
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}


exports.verifyPublication = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined) {
            username = req.user.facebook.name
        }
        Publication.findById(req.body._id, function (err, publication) {
            if (err) {
                console.log("error when finding publication to verify");
                res.send({
                    success: false,
                    message: err
                });
            }
            else if (publication == null) {
                res.send({
                    success: false,
                    message: 'Could not find publication. Maybe it has been rejected?'
                });
            }
            else {
                console.log("Publication: " + publication);

                if (username != publication.versions[0].user) {
                    publication.verified = true;
                    publication.save(function (err) {
                        if (err) {
                            res.send({
                                success: false,
                                message: 'Could not save verification'
                            });
                        }
                        else {
                            Account.findById(publication.addedById, function (err, user) {
                                if (err || user == null) {
                                    console.log("Could not find user who added publication.");
                                }
                                else {
                                    user.game.points += pointsForVerifiedPublication;
                                    user.game.pubAddedAndVerified++;
                                    user.game.eventText.push(pointsForVerifiedPublication + " points for adding a paper which was verified: " + publication.publication_title);
                                    console.log(username + " has " + user.game.points + " points");

                                    user.save(function (err) {
                                        if (err) {
                                        }
                                        else {
                                            console.log(user._id + " was given points as he added a publication which was verified");
                                        }
                                    });
                                }
                            });

                            Account.findById(req.user._id, function (err, userVerifying) {
                                if (err) {
                                    console.log("Could not find user who is verifying publication: " + userVerifying._id);
                                }
                                else {
                                    userVerifying.game.points += pointsForVerifying;
                                    userVerifying.game.pubVerified++;
                                    userVerifying.game.eventText.push(pointsForVerifying + " points for verifying a paper: " + publication.publication_title);
                                    console.log(username + " has " + userVerifying.game.points + " points");

                                    userVerifying.save(function (err) {
                                        if (err) {
                                        }
                                        else {
                                            console.log(userVerifying._id + " was given points as he verified a publication");
                                        }
                                    });
                                }
                            });

                            res.send({
                                success: true,
                                message: 'Publication verified!'
                            });
                        }
                    });
                }
                else {
                    res.send({
                        success: false,
                        message: "You can't verify the publications you added yourselves"
                    });
                }
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.flagPublication = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined) {
            username = req.user.facebook.name
        }

        //Retrieving publication which is to be flagged
        Publication.findById(req.body._id, function (err, publication) {
            if ((err) || (publication == null)) {
                console.log("error when finding publication to delete");
                res.send({
                    success: false,
                    message: 'Invalid papper to flag'
                });
            }
            //Creating new flag object and saving it
            else {

                console.log("Type: " + req.body.type + " || text: " + req.body.text + "addedById: " + req.user._id + " | user: " + username);
                var newFlag = new FlagEvent({
                    type: req.body.type,
                    text: req.body.text,
                    addedById: req.user._id,
                    user: username,
                    resolved : false
                });

                publication.flagged = true;
                publication.flagEvents.push(newFlag);
                publication.save(function (err) {
                    if (err) {
                        console.log(err)
                        res.send({
                            success: false,
                            message: 'Coult not update publication!'
                        });
                    }
                    else {
                        //Rewarding user who flagged publication
                        Account.findById(req.user._id, function (err, flagger) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                flagger.game.points += 2;
                                flagger.game.eventText.push("2 points for flagging " + publication.publication_title);
                                flagger.save(function (err) {
                                    if (err) {
                                        console.log(err)
                                    }
                                });
                            }
                        });

                        if ((publication.addedById != null) && (publication.addedById != "")) {
                            //Finding user who added publication which is to be flagged, in order to notify
                            Account.findById(publication.addedById, function (err, userWhoAddedPublication) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    userWhoAddedPublication.game.eventText.push("A publication you added has been flagged for moderation: " + publication.publication_title);
                                    userWhoAddedPublication.save(function (err) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        else {
                                            console.log("Added of publication notified of flagging");
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            console.log("No 'addedById' on publication");
                        }
                        res.send({
                            success: true,
                            message: 'Flag has been removed!'
                        });
                    }

                });
                console.log(publication.publication_title + " has unflagged");
            }
        });
    }
    else
    {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.removeFlag = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined) {
            username = req.user.facebook.name
        }

        console.log("fei: " + req.body.flagEvent_id);

        //Retrieving publication which is to be flagged
        Publication.findById(req.body._id, function (err, publication) {
            if ((err) || (publication == null)) {
                console.log("error when finding publication to delete");
                res.send({
                    success: false,
                    message: 'Could not find paper'
                });
            }
            else {
                var indexOfFlagEvent = -1;
                var existingFlags = false;

                publication.flagEvents.id(req.body.flagEvent_id).resolved = true;

                for (i = 0; i < publication.flagEvents.length ; i ++){
                    flagEvent = publication.flagEvents[i];
                    //Checking to see if any flags are not resolved
                    if(flagEvent.resolved == false){
                        existingFlags = true;
                    }
                    //Finding flagEvent that is to be marked as resolved
                    if(flagEvent._id == req.body.flagEvent_id){
                        indexOfFlagEvent = i;
                    }
                }
                publication.flagged = existingFlags;

                publication.save(function (err) {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        //Rewarding user who removed flag on publication
                        Account.findById(req.user._id, function (err, flagger) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                flagger.game.points += 2;
                                flagger.game.eventText.push("2 points for removing flag on: " + publication.publication_title);
                                flagger.save(function (err) {
                                    if (err) {
                                        console.log(err)
                                    }
                                });
                            }
                        });

                        if ((publication.addedById != null) && (publication.addedById != "")) {
                            //Finding user who added publication which is to be flagged, in order to notify
                            Account.findById(publication.addedById, function (err, userWhoAddedPublication) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    userWhoAddedPublication.game.eventText.push("A flag on a publication you added has been removed: " + publication.publication_title);
                                    userWhoAddedPublication.save(function (err) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        else {
                                            console.log("Added of publication notified of flagging");
                                        }
                                    });
                                }
                            });
                            Account.findById(publication.flagEvents[indexOfFlagEvent], function (err, userWhoFlaggedPublication) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    userWhoFlaggedPublication.game.eventText.push("The flag you registered on a publication has been removed: " + publication.publication_title);
                                    userWhoFlaggedPublication.save(function (err) {
                                        if (err) {
                                            console.log(err)
                                        }
                                        else {
                                            console.log("Adder of flag notified of removal of flag");
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            console.log("No 'addedById' on publication");
                        }

                    }
                });
                res.send({
                    success: true,
                    message: 'Flag has been removed!'
                });
                console.log(publication.publication_title + " has been flagged");
            }
        });
    }
    else
    {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.deletePublication = function (req, res) {
    if (req.user) {
        var username = req.user.username;
        if (username == undefined){
            username = req.user.facebook.name
        }

        var userWhoAddedPublication;
        Publication.findById(req.body._id, function (err, publication) {
            if (err) {
                console.log("error when finding publication to delete");

            }
            else {
                userWhoAddedPublication = publication.addedById;
            }
        });
        Publication.findByIdAndRemove(req.body._id, function (err, success) {
            if (err) {
                res.send({
                    success: false,
                    message: 'Could not find publication. Maybe it has been rejected already?',
                });
            }
            else {
                Account.findById(userWhoAddedPublication, function (err, user) {
                    if (err || (user == null)) {
                        console.log("Could not find user who added publication: ");
                    }
                    else {
                        user.game.eventText.push("The paper you added (" + req.body.title + ") was rejected by user " + username + ". Reason given: " + req.body.reason);
                        user.save(function (err) {
                            if (err) {
                                console.log("Could not notify the user who added the paper that it has been rejected");
                            }
                            else {
                                console.log( userWhoAddedPublication + " has been notified that the paper he added has been rejected.");
                            }
                        });
                    }
                });

                Account.findById(req.user._id, function (err, userVerifying) {
                    if (err) {
                        console.log("Could not find user who is verifying publication: " + userVerifying._id);
                    }
                    else {
                        userVerifying.game.points += pointsForVerifying;
                        userVerifying.game.pubVerified++;
                        userVerifying.game.eventText.push(pointsForRejectingPublication + " points for rejecting the paper: " + req.body.title );
                        console.log(username + " has " + userVerifying.game.points + " points");

                        userVerifying.save(function (err) {
                            if (err) {
                            }
                            else {
                                console.log(userVerifying._id + " was given points as he rejected a publication");
                            }
                        });
                    }
                });

                res.send({
                    success: true,
                    message: 'Publication removed!'
                });
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.saveMetadata = function (req, res) {
    if (req.user) {
        Publication.findOne({_id: req.body._id}, function (err, publication) {
            if (err) {
                res.send({
                    success: false,
                    message: err
                });
            }
            else if (publication == null){
                res.send({
                    success: false,
                    message: 'Could not find publication'
                });
            }
            //Checking that given values are not empty, and that the value which is to be saved is not the same as current value.
            else if(req.body.key != null && req.body.value != null && (publication[req.body.key] != req.body.key)){
                publication[req.body.key] = req.body.value;
                publication.save(function (err) {
                    if (err) {
                        res.send({
                            success: false,
                            message: 'Could not save verification'
                        });
                    }
                    else {
                        Account.findById(req.user._id, function (err, user) {
                            if (err) {
                                console.log("Could not find user to give points for updating metadata");
                            }
                            else {

                                user.game.points += pointsForMetadata;
                                user.game.metaEdited++;
                                user.game.eventText.push(pointsForMetadata + " points for editing " + req.body.key + " in paper: " + publication.publication_title);
                                user.save(function (err) {
                                    if (err) {
                                        console.log("Could not give points for editing metadata");
                                    }
                                    else {
                                        console.log("Points given to user for editing metadata");
                                    }
                                });
                            }
                        });

                        res.send({
                            success: true,
                            message: req.body.key + ' updated!'
                        });
                    }
                });
            }
            else {
                res.send({
                    success: false,
                    message: 'Nothing to save, or value already saved!'
                });
            }
        });
    }
    else {
        res.send({
            success: false,
            message: 'Unauthorized'
        });
    }
}

exports.findOnePublicationByTitle = function (req, res) { //Utilized in firefox plugin, but this project is on hold for now.
    Publication.findOne({ publication_title: req.params.publication_title}, function (err, results){
        if (err)
            res.send(err)
        if(results){
            res.json(results); // return all todos in JSON format
        }
        else {
            res.send({
                success: false,
                message: 'Publication not found',
            });
        }
    });
};

exports.hitcount = function (req, res) { //Utilized in firefox plugin, but this project is on hold for now.

    var exactMatch = false;
    Publication.findOne({ publication_title: req.params.query}, function (err, results){
        if (err)
            res.send(err)
        if(results){
            exactMatch = true;
        }
    });

    Publication.count(
        {$text: {$search: req.params.query}},
        {score: {$meta: "textScore"}}
    ).exec(function (err, results) {
        if (err) {
            console.log("Error: " + err);
            res.send({
                success: false,
                message: 'Could not count',
            });
        }
        res.send({
            success: true,
            exact_match: exactMatch,
            hitcount: results
        });
    })
}

exports.scrape = function (req, res) { //Used when harvesting publications from DOAJ.
    var noPages = 100;
    var tmp = 10;
    var counter = 0;
    var articles = 0;
    var allRows = [];
    var timeout = 0;
    var pageIndex = 1;

    setInterval(function(){
        request('https://doaj.org/api/v1/search/articles/bibjson.journal.publisher%3A%22Public%20Library%20of%20Science%22?page=' + pageIndex + '&pageSize=100&sort=title%3Aasc', function (error, response, body) {
            if (error) {
                console.log("My print: error with the request to doaj");
                console.log(error);
            }
            var titles = [];
            if (!error && response.statusCode == 200) {
                pageIndex ++;
                var results = [];

                var results = JSON.parse(body)["results"];
                for (var a = 0; a < results.length; a++) {
                    var retrievedPublication = results[a];
                    var retrievedAuthors = retrievedPublication.bibjson.author;
                    var aObj = [];
                    var authorsTMP = "";
                    if (retrievedAuthors) {
                        for (var i = 0; i < retrievedAuthors.length; i++) {
                            aObj.push(retrievedAuthors[i]);
                            if (i === 0) {
                                authorsTMP = retrievedAuthors[i].name;
                            }
                            else {
                                authorsTMP += " ; " + retrievedAuthors[i].name;
                            }
                        }
                    }

                    var retrievedIdentifiers = retrievedPublication.bibjson.identifier;
                    var doiIdentifier = "";
                    for (var i = 0; i < retrievedIdentifiers.length; i++) {
                        // res.send(retrievedIdentifiers[i].id);
                        if (retrievedIdentifiers[i].type == "doi") {
                            doiIdentifier = retrievedIdentifiers[i].id;
                        }
                    }

                    var url = "";
                    if (retrievedPublication.bibjson.link) {
                        url = retrievedPublication.bibjson.link[0].url;
                    }
                    var newVersion = new Version({
                        link: url,
                        type: 'unknown',
                        user: 'OAsearch'
                    })

                    var newPublication = new Publication({
                        verified: 'true',
                        publication_title: retrievedPublication.bibjson.title,
                        abstract: "" + retrievedPublication.bibjson.abstract,
                        authors: authorsTMP,
                        doi: doiIdentifier,
                        ISSN: "",
                        eISSN: "",
                        publisher: retrievedPublication.bibjson.journal.publisher,
                        year: retrievedPublication.bibjson.year,
                        pages: retrievedPublication.bibjson.start_page + " -",
                        journal_title: retrievedPublication.bibjson.journal.title,
                        journal_number: retrievedPublication.bibjson.journal.number,
                        journal_volume: retrievedPublication.bibjson.journal.volume,
                    });


                    newPublication.versions.push(newVersion);
                    newPublication.save(function (err) {
                        if (err) {
                            console.log("My print: error when saving to db");

                            console.log(err);
                        }
                        else {
                            articles++;
                            console.log("Articles: " + articles);
                        }

                    });
                }
                console.log("Requested:  " + (pageIndex * 100));

            }
            else{
                console.log("Error or 200 response / done with 10000");
            }
        });
    }, 1200);

    res.send("done with " + noPages);

};

exports.extendData = function (req, res) { //Used when harvesting publications from DOAJ.
    var noPages = 100;
    var tmp = 10;
    var counter = 0;
    var articles = 0;
    var allRows = [];
    var timeout = 0;
    var pageIndex = 1;

    setInterval(function(){
        request('https://doaj.org/api/v1/search/articles/bibjson.journal.publisher%3A%22Public%20Library%20of%20Science%22?page=' + pageIndex + '&pageSize=100&sort=title%3Aasc', function (error, response, body) {
            if (error) {
                console.log("My print: error with the request to doaj");
                console.log(error);
            }
            var titles = [];
            if (!error && response.statusCode == 200) {
                pageIndex ++;
                var results = [];

                var results = JSON.parse(body)["results"];
                for (var a = 0; a < results.length; a++) {
                    var retrievedPublication = results[a];
                    var retrievedAuthors = retrievedPublication.bibjson.author;
                    var aObj = [];
                    var authorsTMP = "";
                    if (retrievedAuthors) {
                        for (var i = 0; i < retrievedAuthors.length; i++) {
                            aObj.push(retrievedAuthors[i]);
                            if (i === 0) {
                                authorsTMP = retrievedAuthors[i].name;
                            }
                            else {
                                authorsTMP += " ; " + retrievedAuthors[i].name;
                            }
                        }
                    }

                    var retrievedIdentifiers = retrievedPublication.bibjson.identifier;
                    var doiIdentifier = "";
                    for (var i = 0; i < retrievedIdentifiers.length; i++) {
                        // res.send(retrievedIdentifiers[i].id);
                        if (retrievedIdentifiers[i].type == "doi") {
                            doiIdentifier = retrievedIdentifiers[i].id;
                        }
                    }

                    var url = "";
                    if (retrievedPublication.bibjson.link) {
                        url = retrievedPublication.bibjson.link[0].url;
                    }
                    var newVersion = new Version({
                        link: url,
                        type: 'unknown',
                        user: 'OAsearch'
                    })

                    var newPublication = new Publication({
                        verified: 'true',
                        publication_title: retrievedPublication.bibjson.title,
                        abstract: "" + retrievedPublication.bibjson.abstract,
                        authors: authorsTMP,
                        doi: doiIdentifier,
                        ISSN: "",
                        eISSN: "",
                        publisher: retrievedPublication.bibjson.journal.publisher,
                        year: retrievedPublication.bibjson.year,
                        pages: retrievedPublication.bibjson.start_page + " -",
                        journal_title: retrievedPublication.bibjson.journal.title,
                        journal_number: retrievedPublication.bibjson.journal.number,
                        journal_volume: retrievedPublication.bibjson.journal.volume,
                    });


                    newPublication.versions.push(newVersion);
                    newPublication.save(function (err) {
                        if (err) {
                            console.log("My print: error when saving to db");

                            console.log(err);
                        }
                        else {
                            articles++;
                            console.log("Articles: " + articles);
                        }

                    });
                }
                console.log("Requested:  " + (pageIndex * 100));

            }
            else{
                console.log("Error or 200 response / done with 10000");
            }
        });
    }, 1200);

    res.send("done with " + noPages);

};

//mongorestore --batchSize=100 --collection publications --db local publications.bson
//scp -r testDB deploy@82.196.8.172:/usr/www/open-access-search-engine/dumpImport

//Some unfinished logic for server side route authentication. Postponed for now.
/*
 //Set up for gameification
 var pointsForAddingPublication = 10;
 var pointsForVerifiedPublication = 40;
 var pointsForVerifying = 10;
 var pointsForRejectingPublication = 10;
 var pointsForVoting = 2;
 var pointsForMetadata = 2;
 var pointsForVersion = 25;

 //Minimum level to access routes (not implemented for now)
 var addVersionRequirement = 2;
 var addMetadataRequirement = 2;
 var verificationRequirement = 4;
 var editMetadataRequirement = 6;
 var deletePublication = 7;
 var convertPointsToLevel = function(points){ //Used to authenticate actions, but no time to implement.
 if(points < 25) return 1;
 if(points < 50) return 2;
 if(points < 100) return 3;
 if(points < 175) return 4;
 if(points < 350) return 5;
 if(points < 1000) return 6;
 }
 var checkIfAllowed = function (request, user){ //Issues as node as asynch, no time to implement for now.
 var points = user.game.points;
 if ((request == 'addVersion') && (convertPointsToLevel(points) >= addVersionRequirement)) {
 console.log("Calc rank: " + convertPointsToLevel(points) + " -- Requirement: " + addVersionRequirement);
 return true;
 }
 if ((request == 'addMetadata') && (convertPointsToLevel(points) >= addMetadataRequirement)) {
 return true;
 }
 if ((request == 'verifyPublication') && (convertPointsToLevel(points) >= verificationRequirement)) {
 console.log("Calc rank: " + convertPointsToLevel(points) + " -- Requirement: " + verificationRequirement);
 return true;
 }
 if ((request == 'editMetadata') && (convertPointsToLevel(points) >= editMetadataRequirement)) {
 return true;
 }
 if ((request == 'deletePublication') && (convertPointsToLevel(points) >= deletePublication)) {
 return true;
 }
 return false;
 }
 */