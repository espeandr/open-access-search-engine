//CONFIGURATION
//NOTE: need to change facebook base URL on facebook's app management site for FB authorizing to work
//start app with node server.js

var isProduction = true; //Set this to true if running on production environment.

var port;
var database;
var callbackUrl;

var productionPort = 80;
var productionDatabase = 'mongodb://localhost:27017/local';
var productionFacebookCallbackUrl = 'http://www.oasearch.com/auth/facebook/callback';

var localPort = 8080;
var localDatabase = 'mongodb://localhost:27017/testDB'; //Configure to connect to DB
var localFacebookCallbackUrl = 'localhost:8080/auth/facebook/callback';

isProduction ? (port = productionPort, database = productionDatabase, callbackUrl = productionFacebookCallbackUrl) :
    (port = localPort, database = localDatabase, callbackUrl = localFacebookCallbackUrl);
//----------------------------------------------------------------------------------------------------------------------
//Dependencies

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var morgan = require('morgan');             // log requests to the console (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var request = require('request'); //Used for making http requests to external dbs
var routes = require('./routes/index');
var api = require('./routes/api');
var flash = require('connect-flash');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(flash());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'static_HTML_files')));
app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/static_HTML_files'));      // set the static files location /public/img will be /img for users
app.use(express.static(path.join(__dirname, '../client')));
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/app',  express.static(__dirname + '/app'));

//----------------------------------------------------------------------------------------------------------------------
//User authentication

//Using passport-local-mongoose for authentication
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    done(null, user);
});

//Using passport-facebook for authentication
passport.use(new FacebookStrategy({
        clientID        : '613363402161145',
        clientSecret    : 'c40cd6a803e8d190bbfd5ddf5d842ae2',
        callbackURL     : callbackUrl,
        profileFields: ['id', 'name', 'displayName', 'email' ,'picture.type(large)'],
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {
                Account.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);
                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser            = new Account();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.picture = profile.photos ? profile.photos[0].value : '/img/faces/unknown-user-pic.jpg'
                        newUser.facebook.email = profile.emails[0].value;

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
            } else {
                return done(null, req.user);
            }
        });
    }));

//----------------------------------------------------------------------------------------------------------------------
//Routes

//Routes used for passport-facebook authentication.
app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email'}));
app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('#/auth/landing');
    });

//Routes used for passport-local authentication.
app.route('/register').post(routes.register);
app.route('/login').post(routes.login);
app.route('/get_login').get(routes.getLogin);
app.route('/logout').post(routes.logout);

/*
  All other routes: All responses returned in JSON format.
  Rewarding points and registering events handled in called functions.
*/

//Used to fetch users' updated points and events | authenticated required
app.route('/api/user').get(routes.getUpdatedStats);

//Returns publications in JSON | no authentication required
app.route('/api/search/:queryType/:query').get(api.search);

//Returns publications in JSON | authentication required
app.route('/api/verification_list').get(api.getVerificationList);
app.route('/api/flag_list').get(api.getFlagList);

//Returns single publication in JSON | no authentication required
app.route('/api/citationCount').post(api.citationCount); //Forwards a call to Scopus API
app.route('/api/publication/id/:_id').get(api.getPublicationById);
app.route('/api/publication/title/:publication_title').get(api.findOnePublicationByTitle);
app.route('/api/hitcount/:query').get(api.hitcount); //Returns number of hits on query.

//Returns: 'success': boolean, '_id': string (when successful), 'message': string(when failing)
//authentication required
app.route('/api/publication').post(api.createPublication);

//Returns: 'success': boolean, 'message': string | authentication required
app.route('/api/version').post(api.createVersion);
app.route('/api/vote').put(api.voteOnVersion);
app.route('/api/save_metadata').put(api.saveMetadata);
app.route('/api/flag').put(api.flagPublication);
app.route('/api/removeFlag').put(api.removeFlag);
app.route('/api/verify_publication').put(api.verifyPublication);
app.route('/api/delete_publication').delete(api.deletePublication);

app.route('/api/scrapeDOAJ').get(api.scrape);
app.route('/api/checkLink').get(api.checkLink);

var mongosession = mongoose.connect(database);     // connect to mongoDB database on modulus.io

// application -------------------------------------------------------------

app.get('*', function(req, res) {
    res.sendfile('./index.html');
});

// Listed
app.listen(port);
console.log("App listening on port " + port);
console.log("Database in use: " + database);