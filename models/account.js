var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    username: String,
    password: String,
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        picture      : String
    },
    game : {
        points: { type: Number, default: 0 },
        pubAdded: { type: Number, default: 0 },
        pubAddedAndVerified: { type: Number, default: 0 },
        pubVerified: { type: Number, default: 0 },
        pubVoted: { type: Number, default: 0 },
        versionAdded: { type: Number, default: 0 },
        metaEdited: { type: Number, default: 0 },
        eventText: [String]
    }
});

var options = ({missingPasswordError: "Wrong password"});

Account.plugin(passportLocalMongoose, options);

module.exports = mongoose.model('accounts', Account);