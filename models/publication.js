var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Version = new Schema({
    user : String,
    link: String,
    date_added: { type: String, default: getDateTime() },
    type: String,
    upvoting_users: [String],
    downvoting_users: [String],
});

var FlagEvent = new Schema({
    date: { type: String, default: getDateTime() },
    type: String,
    text: String,
    addedById: String,
    user: String,
    resolved: Boolean
});

var Publication = new Schema({
    verified: Boolean,
    date_added: { type: String, default: getDateTime },
    addedById: String,
    flagEvents: [FlagEvent],
    flagged: Boolean,
    publication_title: String,
    abstract: String,
    authors: String,
    publisher: String,
    journal_title: String,
    journal_number: String,
    journal_volume: String,
    year: String,
    pages: String,
    doi: String,
    ISSN: String,
    eISSN: String,
    versions: [Version]
});

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min;

}
//Publication.index({ publication_title: 'text', abstract: 'text'}, {name: 'Full text search index', weights: {publication_title: 5, abstract: 1}});

module.exports = mongoose.model('flagEvents', FlagEvent);
module.exports = mongoose.model('publications', Publication);
