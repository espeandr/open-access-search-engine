var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FlagEvent = new Schema({
    date: { type: String, default: getDateTime() },
    type: String,
    text: String,
    addedById: String,
    user: String,
    resolved: Boolean
});

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min;
}

var FlagEventSchema = FlagEvent;
module.exports = FlagEventSchema;
module.exports = mongoose.model('FlagEvents', FlagEvent);
