angular.module('app').controller('AuthCtrl', ['$scope', '$location','Publications','$window', 'Authentication', function($scope, $location,  Publications, $window, Authentication) {

    Authentication.getLogin().then(function (response){
        console.log(response);
        response = response.data;
        if(response.success == true) {

            Authentication.setCurrentUser(response.user.facebook.name, response.user.facebook.picture);
            Authentication.setAccessLevel('1');
            Authentication.updateStats();
        }
       window.location.href = '/';
    });
}]);
