/**
 * Created by espenandreassen on 28.02.16.
 */



angular.module('app').controller('PublicationCtrl', ['$scope','$rootScope', 'Publications','$stateParams','$location', '$sce','Authentication','$http','smoothScroll', function($scope, $rootScope, Publications, $stateParams, $location, $sce, Authentication, $http, smoothScroll) {


    var element = document.getElementById('navbarID');
    smoothScroll(element);

    $scope.publicationId = $stateParams.publicationId;
    $scope.publication = [];
    $scope.gameUser = Authentication.User;
    $scope.user = Authentication.getCurrentUser();
    $scope.authorizationLevel = Authentication.getAccessLevel();
    $scope.publication.doi = undefined;

    $scope.disqusConfig = {
        disqus_shortname: 'oasearch',
        disqus_identifier: $scope.publicationId,
        disqus_url: $location.absUrl()
    };

    $scope.getCitations = function() {
        Publications.getCitationCount($scope.publication.doi).then(function (response) {
            response = response;
            $scope.citationCount = response.data["search-results"].entry[0]["citedby-count"];
            _altmetric_embed_init();
        });
    }


    Authentication.updateStats();
    $scope.activeFlags = 0;
    $scope.previousFlags = 0;

    $scope.updatePublicationData = function () {
        Publications.getPublicationById($scope.publicationId).then(function (response) {
            $(".dropdown-button").dropdown();
            $(".flagDropdown").dropdown();
            $('ul.tabs').tabs();
            $('select').material_select();
            response = response.data;
            if (response.success == true) {
                $scope.activeFlags = 0;
                $scope.previousFlags = 0;

                $scope.publication = response.publication;
                if($scope.publication.abstract == "" || $scope.publication.abstract == undefined) $scope.publication.abstract = "No abstract registered."

                for (var i = 0; i < $scope.publication.versions.length; i++) {
                    $scope.publication.versions[i].voteSum = ($scope.publication.versions[i].upvoting_users.length -
                    $scope.publication.versions[i].downvoting_users.length);

                    if (($scope.topVersion == undefined) || ($scope.publication.versions[i].voteSum > $scope.topVersion.voteSum)) {
                        $scope.topVersion = $scope.publication.versions[i];
                    }
                }
                if (($scope.publication.doi != "") && ($scope.publication.doi != undefined)){
                    $scope.getCitations();
                }
                $scope.$on('$viewContentLoaded', function(){
                    _altmetric_embed_init();
                });
                for (var i = 0; i < $scope.publication.flagEvents.length; i++) {
                    if($scope.publication.flagEvents[i].resolved == true){
                        console.log("previous")
                        $scope.previousFlags ++;
                    }
                    if($scope.publication.flagEvents[i].resolved == false){
                        console.log("active")

                        $scope.activeFlags ++;
                    }
                }
            }
            else {
                $location.path('/');
            }
        });
    }
    $scope.updatePublicationData();


    $scope.altmetricURL = "https://api.elsevier.com/content/abstract/citation-count?doi=" + $scope.publication.doi + "&apiKey=04ddf748c808d2ac52b8c9e71733ba7e";

    $scope.goToVerificationList = function(){
        if ($scope.authorizationLevel > 0){
            $location.path('/verification');
        }
        else {
            $location.path('/verification');
        }
    }

    $scope.verifyPublication = function (){
        Publications.verifyPublication($scope.publicationId).then(function (response){
            response = response.data;
            if(response.success == true){
                $location.path('/publication/' + $scope.publicationId);
                $rootScope.$broadcast("event", {'message': 'Thank you for your contribution! The user who added this publication has been notified that you accepted this publication.', 'points': 10, 'title': 'Thank you!'});
                Authentication.updateStats();
                $scope.publication.verified = "true";
            }
            Materialize.toast(response.message, 5000);
        });
    }

    $scope.checkIfShouldBeDisplayed = function(key, value){
        if (key == "_id" || key == "__v" || key == ""|| key == "versions" || key == "link" || key == "verified" || value == "" || key == "addedById" || key == "date_added"){
            return true;
        }
        return false;
    }

    $scope.collapsibleIcon = "expand_more";
    $scope.changeIcon = function () {
        if ($scope.collapsibleIcon === "expand_more") {
            $scope.collapsibleIcon = "expand_less";
        }
        else {
            $scope.collapsibleIcon = "expand_more";
        }
    };

    $scope.goToModifyMetadata = function(){
        if ($scope.authorizationLevel >= 1){
            $location.path('/modify_metadata/' + $scope.publicationId);
        }
        else {
            Materialize.toast("You need to be signed in to modify metadata.")
        }
    }


    $scope.openFlagForModerationModal = function(){
        $('#flagModal').openModal();
    }

    $scope.openRemoveFlagModal = function(_id){
        $scope.FlagEventIDtoBeRemoved = _id
        $('#removeFlagModal').openModal();
    }

    $scope.removeFlag = function(){
        Publications.removeFlagForModeration($scope.publication._id, $scope.FlagEventIDtoBeRemoved).then(function(response){
            response = response.data;
            if(response.success == true){
                $scope.updatePublicationData();
                Authentication.updateStats();
                $scope.updatePublicationData();
            }
            Materialize.toast(response.message, 3000)
            $('#removeFlagModal').closeModal();
        })

    }

    $scope.flagForModeration = function(){
        if (($scope.flagReason == "") || ($scope.flagReason == undefined) || ($scope.flagType == "") || ($scope.flagType == undefined)){
            Materialize.toast("Please fill in both fields", 3000);
        }
        else{
            $('#flagModal').closeModal();
            Publications.flagForModeration($scope.publication._id, $scope.flagType, $scope.flagReason).then(function (response) {
                response = response.data;

                if (response.success == true) {
                    Materialize.toast("Publication flagged", 3000);
                    $scope.publication.flagged = true;
                    $scope.updatePublicationData();
                    Authentication.updateStats();
                }
                else {
                    Materialize.toast(response.message, 4000);
                }
            });
            $scope.flagReason = "";
            $scope.flagType = 'Uncategorized flag';
        }
    }
    $scope.flagType = "Uncategorized flag";
    $scope.flagReason = "";

    $scope.panelWidth = "col s12 l10 offset-l2"
    $scope.altmetricFound = false;

    $scope.showFlags = function (){
        $scope.disableFilter = true;
        $scope.publication.flagged = true;
    }

    $(function () {
        $('div.altmetric-embed').on('altmetric:show', function () {
            $scope.panelWidth = "col s12 l10"
            $scope.altmetricFound = true;
        });
    });

    angular.element(document).ready(function () {
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $(".dropdown-button").dropdown();
        $(".flagDropdown").dropdown();
        $('ul.tabs').tabs();
        $('select').material_select();
    });

}]);


angular.module('app').directive('titleLink', function() {
    return {
        restrict: 'AE',
        replace: true,
        template: '<a href={{publication.link}} class="blue-grey-text title">{{publication.publication_title}}<i class="material-icons icon-link blue-grey-text tiny">open_in_new</i></a>'

    }
});

angular.module('app').directive('addVersion',['Publications', 'smoothScroll','$rootScope','Authentication', function(Publications, smoothScroll, $rootScope, Authentication) {
    return {
        restrict: 'AE',
        replace: true,
        template: "<div><a ng-click='openVersionModal()' class='{{votingEnabled}} right waves-effect waves-light remove-shadow btn blue-grey white-text'><i class='material-icons right white-text'>note_add</i>Add a version</a>" +
        "<div id='versionModal' class='modal publication-modal modal-fixed-footer' style='min-height : 30%'>" +
        "<form class='modal-content'>" +
        "<h5 class='blue-grey-text'>Please use a link to the landing page of the paper<br> you wish to add!</h5>" +
        "<div class='row'>" +
        "<form name='versionForm'>"+
        "<div class='input-field col s12'><input ng-model='formLink' placeholder='Please paste a link to the paper' id='link' type='text' class='validate' required><label class='active' for='link'>link to the paper</label></div>" +
        "</form>" +
        "<div class='row'>" +
        "<div class='col s5 grey-text'>" +
        "<span class='grey-text' style='font-size: 0.8rem;'> Select a predefined type, or specify yourselves</span>" +
        "<select ng-change='checkVersion()' class='browser-default' ng-model='typeSelect'>" +
        "<option value=''>optional" +
        "<option value='version of record'> version of record" +
        "<option value='draft'>draft" +
        "<option value='specify'>specify" +
        "<option value='unknown'>unknown" +
        "</select>" +
        "</div>" +
        "<div ng-switch='typeSelect'>" +
        '<div ng-switch-when="specify" class="input-field col s7">' +
        "<input ng-model='$parent.selfTyped' placeholder='Write a short description of type' id='type' type='text' class='validate'><label class='active' for='type'>type of version</label>" +
        "</div></div>{{$parent.selfTyped}}</div>" +

        "</div>" +
        "<div class='modal-footer blue-grey lighten-2'>" +
        "<div class='row no-margin-bottom'>" +
        "<div class='col l6 s12 white-text' style='padding-left: 20px'>{{validationMessage}}</div>" +
        "<div class='col l2 s6 offset-l2'> <a class='right waves-effect waves-light remove-shadow btn modal-action modal-close publication-schema-button blue-grey white-text'>" +
        "<i class='material-icons publication-schema-button-icon right white-text'>close</i>cancel</a></div>" +
        "<div class='col l2 s6'> <a ng-class='{disabled : versionForm.$invalid}' ng-click='submit()' class='publication-schema-button right waves-effect waves-light remove-shadow btn blue-grey white-text'>" +
        "<i class='material-icons publication-schema-button-icon right white-text'>send</i>submit</a></div>" +
        "</div></div>",

        link: function (scope) {

            scope.validationMessage = 'Pasting a link is mandatory';

            scope.authorizationLevel = Authentication.getAccessLevel();

            if(scope.authorizationLevel === undefined) {
                scope.votingEnabled = "disabled";
                scope.authorizationLevel = -1;
            }

            scope.formLink = ""
            scope.typeSelect = "";
            scope.selfTyped = "";
            scope.openVersionModal = function() {
                if(scope.authorizationLevel < 1){
                    Materialize.toast("You need to be signed in to add a version.", 20000);
                }
                else {
                    $('#versionModal').openModal();
                }
            }

            scope.submit = function(){
                if(scope.authorizationLevel < 1){
                    Materialize.toast("You need to be signed in to add a version.", 2000);
                }
                else{
                    if((scope.formLink != "" || scope.formLink == undefined)){
                        if (scope.typeSelect === "specify"){
                            scope.type = scope.selfTyped;
                        }
                        else{
                            scope.type = scope.typeSelect;
                        }

                        Publications.createVersion(scope.formLink, scope.type, scope.publication._id).then(function(response) {
                            response = response.data;
                            Materialize.toast(response.message, 3000);
                            if(response.success == true){
                                scope.updatePublicationData();
                                Authentication.updateStats();
                                $rootScope.$broadcast("event", {'message': 'Thank you for adding a source / version! Maybe you would like to vote on the one you just added?', 'points': 25, 'title': 'Thank you!'});
                                var indexOfLastVersion = "" + (scope.publication.versions.length - 2);
                                var element = document.getElementById(indexOfLastVersion);
                                smoothScroll(element);
                                scope.typeSelect = "";
                                scope.type = "";
                                scope.selfTyped ="";
                                scope.link = "";
                                $('#versionModal').closeModal();
                            }
                        });
                    }
                    else{
                        scope.validationMessage = "Please input a link!";
                    }
                }
            }
        }
    }
}]);


angular.module('app').directive('voteButton',['Publications','Authentication', function(Publications, Authentication) {
    return {
        restrict: 'AE',
        scope: {pub: '=', pid: '=', vid: '=', type: '='},
        template: "<a ng-disabled='!authorizationLevel' class='{{votingEnabled}} right waves-effect waves-light remove-shadow btn blue-grey white-text' " +
        "ng-click='registerVote()'><i class='material-icons right white-text' style='margin-left: 0px'>{{icon_name}}</i>{{vote_text}}</a>",

        link: function (scope) {

            scope.authorizationLevel = Authentication.getAccessLevel();

            if(scope.authorizationLevel === undefined) {
                scope.votingEnabled = "disabled";
                scope.authorizationLevel = -1;
            }

            //Giving buttons correct icon and text
            if(scope.type == "upvote"){
                scope.icon_name = "thumb_up";
                scope.vote_text = "vote up"
            }
            else{
                scope.icon_name = "thumb_down";
                scope.vote_text = "vote down"
            }

            //Some logic to check for and remove potential previous votes on client side.
            var indexOfVersion = null;
            for(var i = 0; i < scope.pub.versions.length; i ++) {
                if (scope.pub.versions[i]._id == scope.vid) {
                    indexOfVersion = i;
                }
            }

            scope.registerVote = function(){

                if(scope.authorizationLevel < 1){
                    Materialize.toast("You need to be logged in to vote", 20000);
                }
                else{
                    if((scope.pub.versions[indexOfVersion].upvoting_users.indexOf(Authentication.getCurrentUser()) > -1)&&
                        scope.type == "upvote"){
                        Materialize.toast("You have already voted this up.", 3000);
                    }
                    else if((scope.pub.versions[indexOfVersion].downvoting_users.indexOf(Authentication.getCurrentUser()) > -1)&&
                        scope.type == "downvote"){
                        Materialize.toast("You have already voted this down.", 3000);
                    }
                    else {
                        Publications.voteOnVersion(scope.pid, scope.vid, scope.type).then(function (response){
                            response = response.data;
                            Materialize.toast(response.message, 5000);
                            if(response.success == true) {
                                Authentication.updateStats();
                                if (scope.type == "upvote") {
                                    scope.pub.versions[indexOfVersion].downvoting_users.pop();
                                    scope.pub.versions[indexOfVersion].upvoting_users.push(Authentication.getCurrentUser());
                                }
                                else {
                                    scope.pub.versions[indexOfVersion].upvoting_users.pop();
                                    scope.pub.versions[indexOfVersion].downvoting_users.push(Authentication.getCurrentUser());
                                }
                            }
                        });
                    }
                }

            }
        }
    }
}]);
