/**
 * Created by espenandreassen on 25.02.16.
 */


var app = angular.module('app', ['ui.router', 'ngAnimate', 'ngCookies', 'angular-loading-bar', 'ngSanitize', 'smoothScroll', '720kb.tooltips', 'angularUtils.directives.dirDisqus',]);
app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider','$compileProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider) {
    $httpProvider.interceptors.push('authHttpResponseInterceptor');//Http Intercpetor to check auth failures for xhr requests
    $urlRouterProvider.otherwise('/home');
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel|data:image\|app|):/);

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'app/main/main.html',
            controller: 'IndexCtrl',
        })
        .state('authenticationLanding',{
            url: '/auth/landing',
            templateUrl: 'app/auth/auth.html',
            controller: 'AuthCtrl'
        })
        .state ('search',{
            url: '/search?queryType?query',
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl',
        })
        .state ('profile',{
            url: '/profile',
            templateUrl: 'app/profile/profile.html',
            controller: 'ProfileCtrl',
        })
        .state ('features',{
            url: '/features',
            templateUrl: 'app/features/features.html',
            controller: 'FeaturesCtrl',
        })
        .state ('publication',{
            url: '/publication/:publicationId',
            templateUrl: 'app/publication/publication.html',
            controller: 'PublicationCtrl',
        })
        .state ('verification',{
            url: '/verification',
            templateUrl: 'app/verification/verification.html',
            controller: 'VerificationCtrl'
        })
        .state ('flags',{
            url: '/flags',
            templateUrl: 'app/flags/flags.html',
            controller: 'FlagsCtrl'
        })
        .state ('add_publication',{
            url: '/add_publication',
            templateUrl: 'app/add_publication/add_publication.html',
            controller: 'AddPublicationCtrl'
        })
        .state ('modify_metadata',{
            url: '/modify_metadata/:publicationId',
            templateUrl: 'app/modify_metadata/modify_metadata.html',
            controller: 'ModifyMetadataCtrl'
        })
}]);



//Intercepting 401 responses
app.factory('authHttpResponseInterceptor',['$q',function($q){
    return {
        response: function(response){
            if (response.status === 401) {
            }
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            if (rejection.status === 401) {
                Materialize.toast("Unauthorized", 5000);
            }
            return $q.reject(rejection);
        }
    }
}]);

//Holds all logic for authentication, current user and user's stats
app.factory('Authentication', function($http, $cookies, $rootScope, $window) {
    var User = {
        'username': "",
        'game' : {
            'points': 0,
            'pubAdded': 0,
            'pubAddedAndVerified': 0,
            'pubVerified': 0,
            'pubVoted': 0,
            'versionAdded': 0,
            'metaEdited': 0,
            'level': 0,
            'progressToNextLevel': 0,
            'pointsToNextLevel': 0,
            'nextLevelRequires': 0,
            'nextUnlock': "", //A text stating what is unlocked as the next level is reached
            'eventText': [] //All events related to user (some points awarded, publication verified...)
        }
    };

    // Updates User object based on current points
    var updateGameProgress = function(points) {
        switch (true) {
            case points < 25:
                User.game.progressToNextLevel = points / 25 * 100;
                User.game.level = 1;
                User.game.pointsToNextLevel = 25 - points;
                User.game.nextLevelRequires = 25;
                User.game.nextUnlock = "Add a source / version to a paper + add missing metadata";
                break;

            case points < 50:
                User.game.progressToNextLevel = (points - 25) / 25 * 100;
                User.game.level = 2;
                User.game.pointsToNextLevel = 50 - points;
                User.game.nextLevelRequires = 50;
                User.game.nextUnlock = "Flag papers for moderation";
                break;

            case points < 100:
                User.game.progressToNextLevel = (points - 50) / 50 * 100;
                User.game.level = 3;
                User.game.pointsToNextLevel = 100 - points;
                User.game.nextLevelRequires = 100;
                User.game.nextUnlock = "Verify and reject submitted papers";
                break;

            case points < 150:
                User.game.progressToNextLevel = (points - 100) / 50 * 100;
                User.game.level = 4;
                User.game.pointsToNextLevel = 150 - points;
                User.game.nextLevelRequires = 150;
                User.game.nextUnlock = "Edit existing metadata fields";
                break;

            case points < 250:
                User.game.progressToNextLevel = (points - 150) / 100 * 100;
                User.game.level = 5;
                User.game.pointsToNextLevel = 250 - points;
                User.game.nextLevelRequires = 250;
                User.game.nextUnlock = "View all flagged publications + remove flags for moderation";
                break;

            case points < 1000:
                User.game.progressToNextLevel = (points - 350) / 650 * 100;
                User.game.level = 6;
                User.game.pointsToNextLevel = 1000 - points;
                User.game.nextLevelRequires = 1000;
                User.game.nextUnlock = "Delete a paper";
                break;

            case points > 1000:
                User.game.progressToNextLevel = 100;
                User.game.level = 7;
                User.game.pointsToNextLevel = 0;
                User.game.nextLevelRequires = 0;
                User.game.nextUnlock = "No more unlocks created yet";
                break;
        }
    };

    //Takes a User object, checks if he has leveled up, calls function for setting/updating new User object
    var putUserObject = function (updatedUser) {
        if((convertPointsToLevel(updatedUser.game.points) != User.game.level) && (User.game.level != 0)){
            var tmp = User.game.level;
            tmp ++;
            $rootScope.$broadcast("levelUp", {'level': (tmp), 'unlock': User.game.nextUnlock});
        }
        User.game = updatedUser.game;
        User.username = updatedUser.username;
        updateGameProgress(updatedUser.game.points);
    };

    var convertPointsToLevel = function(points){
        if(points < 25) return 1;
        if(points < 50) return 2;
        if(points < 100) return 3;
        if(points < 150) return 4;
        if(points < 250) return 5;
        if(points < 1000) return 6;
        if(points > 1000) return 7;
    }; //Used to find out what level user is on, based on points
    var getLogin = function() {
        return $http.get('/get_login');
    };

    var getAccessLevel = function (){
        if($cookies.get('level')) {
            return $cookies.get('level')
        }
        else {
            return 0;
        }
    };

    var logout = function() {
        var req = {
            method: 'POST',
            url: '/logout',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        };
        $http(req).then(function (response) {
            $cookies.remove('currentUser');
            $cookies.remove('level');
            $cookies.remove('picture');
            $cookies.remove('connect.sid');
            $window.location.reload();
        });
    };
    return {
        User: User,
        login : function(_username, _password) {
            var req = {
                method: 'POST',
                url: '/login',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({username: _username, password: _password})
            };
            return $http(req);
        },
        getLogin : getLogin,
        register : function(_username, _password) {
            var req = {
                method: 'POST',
                url: '/register',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({username: _username, password: _password})
            };
            return $http(req);
        },
        logout : logout,

        setCurrentUser: function (username, picture) {
            $cookies.put('currentUser', username);
            $cookies.put('picture', picture);
        },
        putUserObject: putUserObject,
        getCurrentUser: function(){
            var pic = "account_box"; //Use standard icon if no fb picture is available
            if ($cookies.get('picture')) {
                pic = $cookies.get('picture');
            }
            if ($cookies.get('currentUser')){
                return {
                    username: $cookies.get('currentUser'),
                    picture: pic
                }
            }
            else{
                return false;
            }
        },
        newEventCookieRegistered: function(){ //This is a real dirty fix for giving users welcome message. Fix when time.
            $cookies.put('registered', 'true')
        },
        welcomeGiven: function(){
            if($cookies.get('registered')) {
                if($cookies.get('registered') == 'true'){
                    $cookies.put('registered', 'false');
                    return false;
                }
                return true;
            }
        },
        setAccessLevel: function(level){
            $cookies.put('level', level)
        },
        getAccessLevel: getAccessLevel,
        goToVerificationList : function (){
            $location.path('/verify_publications');
        },
        updateStats: function (){
            $http.get('/api/user').then(function(response) {
                response = response.data;
                if(response.success == true) {
                    putUserObject(response.user);
                }
                if (response.success == false) {
                    if(getAccessLevel() != 0){
                        console.log("You have been signed out.")
                        logout();
                    }
                }
            });
        }
    }
});

console.log("a test");

//Holds all api calls related to publications
app.factory('Publications', function($http) {
    return {
        search : function(field, query) {
            var encoded = encodeURIComponent(query);
            return $http.get('/api/search/' + field + '/' + encoded);
        },
        getCitationCount : function(doi){
            var req = {
                method: 'POST',
                url: '/api/citationCount',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({doi: doi})
            };
            return $http(req);
        },
        getPublicationById : function(_id) {
            return $http.get('/api/publication/id/' + _id);
        },
        createPublication : function(p_link, p_title, p_authors, p_abstract, p_doi, p_publisher, p_journal_title, p_year, p_pages, p_ISSN, p_eISSN, p_type) {
            var req = {
                method: 'POST',
                url: '/api/publication',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({link: p_link, title: p_title, authors: p_authors, abstract: p_abstract, doi: p_doi, publisher: p_publisher,
                    journal_title: p_journal_title, year: p_year, pages: p_pages, ISSN: p_ISSN, eISSN: p_eISSN, type: p_type})
            };
            return $http(req);
        },
        createVersion : function(v_link, v_type, v_id) {
            var req = {
                method: 'POST',
                url: '/api/version',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({link: v_link, type: v_type, _id: v_id})
            };
            return $http(req);
        },
        flagForModeration : function(p_id, flagType, flagReason){
            var req = {
                method: 'PUT',
                url: '/api/flag',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({_id: p_id, type: flagType, text: flagReason})
            };
            return $http(req);
        },
        removeFlagForModeration : function(p_id, FlagEvent_id){
            var req = {
                method: 'PUT',
                url: '/api/removeFlag',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({_id: p_id, flagEvent_id: FlagEvent_id})
            };
            return $http(req);
        },
        voteOnVersion : function(p_id, v_id, vote_type) {
            var req = {
                method: 'PUT',
                url: '/api/vote',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({publication_id: p_id, version_id: v_id, type: vote_type})
            };
            return $http(req);
        },
        verifyPublication : function(p_id) {
            var req = {
                method: 'PUT',
                url: '/api/verify_publication',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({_id: p_id})
            };
            return $http(req);
        },
        saveMetadata : function(p_id, key, value) {
            var req = {
                method: 'PUT',
                url: '/api/save_metadata',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({_id: p_id, key: key, value: value})
            };
            return $http(req);
        },
        deletePublication : function(p_id, p_title, rejectionReason) {
            var req = {
                method: 'DELETE',
                url: '/api/delete_publication',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({_id: p_id, title: p_title, reason: rejectionReason})
            };
            return $http(req);
        },
        getVerificationList : function() {
            return $http.get('/api/verification_list/');
        },
        getFlagList : function() {
            return $http.get('/api/flag_list/');
        },

        count : function(query) {
            return $http.get('/api/hitcount/' + query);
        }
    }
});



