/**
 * Created by espenandreassen on 28.02.16.
 */
angular.module('app').controller('VerificationCtrl', ['$scope', '$rootScope', '$location','Publications','$window', 'Authentication', function($scope, $rootScope, $location,  Publications, $window, Authentication){
    $location.search('query', null);
    $scope.publications = [];
    $scope.gameUser = Authentication.User;
    Authentication.updateStats();

    $scope.authorizationLevel = Authentication.getAccessLevel();
    if($scope.authorizationLevel < 1){
        Materialize.toast("You need to be signed in to use this functionality", 5000);
    }

    $scope.listRetrived = false;

    Publications.getVerificationList().success(function(data) {
        if(data.success != false){
            $scope.publications = data;
            //Deleting all field that is not to be shown to user + empty feilds
            for (i = 0; i < $scope.publications.length; i ++){
                $scope.publications[i].user = $scope.publications[i].versions[0].user;
                $scope.publications[i].link = $scope.publications[i].versions[0].link;
                $scope.publications[i].show = true;

                //   delete $scope.publications[i]._id;
                delete $scope.publications[i].verified;
                delete $scope.publications[i].addedById;
                delete $scope.publications[i].__v
                delete $scope.publications[i].versions;

                for (var a in $scope.publications[i]) {
                    if ($scope.publications[i][a] === null || $scope.publications[i][a] === "") {
                        delete $scope.publications[i][a];
                    }
                }
            }
            $scope.listRetrived = true;
        }
    });

    $scope.removePublication = function (publication_id){
        for (i = 0; i < $scope.publications.length; i ++){
            if($scope.publications[i]._id == publication_id){
                $scope.publications[i].show = false;
            }
        }
    }

    $scope.checkIfShouldBeDisplayed = function(key, value){
        if (key == "_id" || key == "publication_title" || key == "date_added" || key == "user" || key == "show"  || key == "__v" || key == "versions"  || key == "verified" || value == ""){
            return true;
        }
        return false;
    }

    $scope.openInclusionGuidelines = function (){
        $rootScope.$broadcast("inclusionGuidelines");
    }
    //An modal opens when user clicks reject/accept and the publication in question is saved to a variable.
    //If the user verifies his choice, the publication in question is deleted/verified.
    $scope.openDeletePublicationModal =  function(publication_id, publication_title){
        $scope.toBeDeleted = publication_id;
        $scope.toBeDeletedTitle = publication_title;

        $('#rejectPublicationModal').openModal();
    }
    $scope.openVerifyPublicationModal =  function(publication_id){
        $scope.toBeVerified = publication_id;
        $('#verifyPublicationModal').openModal();
    }
    // Called on delete confirmation
    $scope.deletePublication =  function(){
        if(($scope.rejectionReason == "") || ($scope.rejectionReason == undefined)){
            $scope.rejectionReason = "No feedback given.";
        }
        Publications.deletePublication($scope.toBeDeleted, $scope.toBeDeletedTitle, $scope.rejectionReason).then(function (response){
            response = response.data;
            if(response.success == true){
                if($scope.rejectionReason == "") $scope.rejectionReason = "No feedback given.";

                $scope.removePublication($scope.toBeDeleted);
                $('#rejectPublicationModal').closeModal();
                Authentication.updateStats();
                $rootScope.$broadcast("event", {'message': 'Thank you for your effort! The user who added this publication has been notified that you rejected this publication with the following statement: ' + $scope.rejectionReason, 'points': 10, 'title': 'Thank you!'});
                $scope.rejectionReason = "";

            }
            else{
                Materialize.toast(response.message, 5000);
            }
        });
    }
    // Called on verify confirmation
    $scope.verifyPublication = function (){
        Publications.verifyPublication($scope.toBeVerified).then(function (response){
            response = response.data;
            if(response.success == true){
                $location.path('/publication/' + $scope.toBeVerified);
                $('#verifyPublicationModal').closeModal();
                Authentication.updateStats();
                $rootScope.$broadcast("event", {'message': 'Thank you for your contribution! The user who added this paper has been notified that you accepted it.', 'points': 10, 'title': 'Thank you!'});
            }
            Materialize.toast(response.message, 5000);
        });
    }

    //Used to open publication in a new tab
    $scope.redirectTo = function(url){
        $window.open(url, '_blank');
    };


    $(document).ready(function(){
        $(".dropdown-button").dropdown();
        $('.collapsible').collapsible({
            accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    });
}]);
