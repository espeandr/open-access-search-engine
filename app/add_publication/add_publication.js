/**
 * Created by espenandreassen on 12.04.16.
 */
/**
 * Created by espenandreassen on 28.02.16.
 */
angular.module('app').controller('AddPublicationCtrl', ['$scope','$rootScope', '$location','Publications','$window', '$sce','Authentication', function($scope, $rootScope, $location,  Publications, $window ,$sce, Authentication){

    // $scope.addPublication.link.$setDirty();

    $scope.userStats = Authentication.User;
    $scope.autoDetected = false;


    $scope.authorizationLevel = $scope.userStats.game.level;
    if($scope.authorizationLevel < 1){
        Materialize.toast("You need to be signed in add a paper", 5000);
    }

    if($scope.authorizationLevel == undefined) {
        $scope.publicationAddingEnabled = "disabled";
        $scope.authorizationLevel = -1;
    }

    $scope.retrieveByDOI = function (){
        var settings = {
            "async": true,
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Key',
            "crossDomain": true,
            "url": "http://api.crossref.org/works/http:/" + $scope.doi,
            "method": "GET",
            "error" : function(jqXHR, textStatus, errorThrown) {
                if(jqXHR.status == 404 || errorThrown == 'Not Found')
                {
                    Materialize.toast("Could not find paper by DOI. Perhaps you mistyped it?", 3000);
                }
            },
            "headers": {
            }
        }
        $.ajax(settings).done(function (response) {
            $scope.authors = "";


            var response = response.message;
            $scope.title = response.title[0];
            $scope.ISSN = response.ISSN[0];
            $scope.link1 = response.URL;
            $scope.journal_title = response["container-title"][0];
            $scope.pages = response.page;
            $scope.publisher = response.publisher;
            $scope.year = response["created"]["date-parts"][0][0];
            $scope.version = "test"
            $scope.typeSelect = "version of record"

            for (var i = 0; i < response.author.length; i ++ ){
                $scope.authors += response.author[i]["given"] + " ";
                if(i != response.author.length - 1) {
                    $scope.authors += response.author[i]["family"] + " ; ";
                }
                else{
                    $scope.authors += response.author[i]["family"];
                }
            }
            $scope.doiCompleteMessage = 'Paper was found. IMPORTANT: Make sure that the link provided bellow leads to a version which is accessible outside any university or institutional networks. Please make sure all fields are correct, and fill inn as many missing fields as possible before submitting.'
            $scope.autoDetected = true;
            $scope.$apply();
        });
    }

    $scope.link = "";
    $scope.link1 = "";
    $scope.title = "";
    $scope.authors = "";
    $scope.abstract = "";
    $scope.doi = "";
    $scope.publisher = "";
    $scope.journal_title = "";
    $scope.year = "";
    $scope.pages = "";
    $scope.ISSN = "";
    $scope.eISSN = "";

    $scope.openInclusionGuidelines = function (){
        $rootScope.$broadcast("inclusionGuidelines");
    }


    $scope.submitPublication = function() {
        if ($scope.autoDetected == true){
            $scope.utilizedLink = $scope.link1
        }
        else{
            $scope.utilizedLink = $scope.link;
        }


        if($scope.authorizationLevel == "tmpfortest"){
            Materialize.toast("You are not authorized to add a publication", 3000);
        }
        else {
            if (($scope.title != "") && ($scope.utilizedLink != "")) {
                var type = $scope.typeSelect;
                if ($scope.typeSelect == "specify"){
                    type = $scope.selfTyped;
                }
                Publications.createPublication($scope.utilizedLink, $scope.title, $scope.authors, $scope.abstract,
                    $scope.doi, $scope.publisher, $scope.journal_title, $scope.year, $scope.pages, $scope.ISSN, $scope.eISSN, type).then(function (response) {
                    response = response.data;

                    Materialize.toast(response.message, 5000);
                    if (response.success != false) {
                        $rootScope.$broadcast("event", {'message': 'Thank you for your contribution! The paper you just added is awaiting verification by another user. ' +
                        'If the paper you submitted is verified, you will be awarded another 40 points!', 'points': 10, 'title': 'Thank you!'});

                        $location.url('/publication/' + response._id);
                    }
                });
            }
            else {
                $scope.validationMessage = "Title and link are mandatory!";
            }
        }
    }

    $(document).ready(function() {
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $(".dropdown-button").dropdown();
        $(".flagDropdown").dropdown();
        $('ul.tabs').tabs();
        $('select').material_select();
    });
}]);


