/**
 * Created by espenandreassen on 28.02.16.
 */
angular.module('app').controller('FlagsCtrl', ['$scope','Publications','Authentication', function($scope, Publications, Authentication){

    Authentication.updateStats();
    $scope.publications = [];

    $scope.updatelist = function(){
        Publications.getFlagList().then(function(response) {
            response = response.data;
            console.log(response)
            if(response.success != false){
                $scope.publications = response.publications;
                console.log($scope.publications)
                //Deleting all field that is not to be shown to user + empty feilds
                for (i = 0; i < $scope.publications.length; i ++){
                }
                $scope.listRetrived = true;
            }
            else {
                Materialize.toast("You have to be level 6 to to view all flagged publications")
            }
        });
    }

    $scope.authorizationLevel = Authentication.getAccessLevel();
    if($scope.authorizationLevel < 1 || (Authentication.User.game.level <= 6)){
        Materialize.toast("You need to be signed in to use this functionality", 5000);
    }
    else {
        $scope.updatelist();
    }

    $(document).ready(function(){
        $('.collapsible').collapsible({
            accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $(".dropdown-button").dropdown();
    });
}]);
