/**
 * Created by espenandreassen on 12.04.16.
 */
/**
 * Created by espenandreassen on 28.02.16.
 */
angular.module('app').controller('ModifyMetadataCtrl', ['$scope', '$location', '$stateParams','Publications','$window', '$sce', 'Authentication', function($scope, $location, $stateParams,  Publications, $window ,$sce, Authentication){


    $scope.authorizationLevel = Authentication.getAccessLevel();
    if($scope.authorizationLevel < 1){
        Materialize.toast("You need to be signed in to use this functionality", 5000);
    }

    $scope.publicationId = $stateParams.publicationId;
    $scope.verified
    $location.search('query', null);
    $scope.publication = [];
    $scope.gameUser = Authentication.User;
    Authentication.updateStats();

    Publications.getPublicationById($scope.publicationId).success(function(data) {

        $scope.publication = data.publication;
        $scope.addedByUser = $scope.publication.versions[0].user;
        $scope.deletionTitle = $scope.publication.publication_title;
        $scope.deletionID = $scope.publication._id;


        if($scope.publication.abstract == undefined) $scope.publication.abstract = "";
        if($scope.publication.authors == undefined) $scope.publication.authors = "";
        if($scope.publication.publisher == undefined) $scope.publication.publisher = "";
        if($scope.publication.journal_title == undefined) $scope.publication.journal_title = "";
        if($scope.publication.journal_volume == undefined) $scope.publication.journal_volume = "";
        if($scope.publication.year == undefined) $scope.publication.year = "";
        if($scope.publication.pages == undefined) $scope.publication.pages = "";
        if($scope.publication.doi == undefined) $scope.publication.doi = "";
        if($scope.publication.ISSN == undefined) $scope.publication.ISSN = "";
        if($scope.publication.eISSN == undefined) $scope.publication.eISSN = "";



        //So that html tags are rendered as html
        $scope.title = $sce.trustAsHtml($scope.publication.publication_title);
        $scope.verified = $scope.publication.verified;


        //Used for finding top rated version, and thus correct link
        for (var i = 0; i < $scope.publication.versions.length; i++) {
            $scope.publication.versions[i].voteSum = ($scope.publication.versions[i].upvoting_users.length -
            $scope.publication.versions[i].downvoting_users.length);

            if (($scope.topVersion == undefined) || ($scope.publication.versions[i].voteSum > $scope.topVersion.voteSum)) {
                $scope.topVersion = $scope.publication.versions[i];
            }
        }

        //Deleting all fields that is not to be shown to user
        delete $scope.publication._id;
        delete $scope.publication.eventText;
        delete $scope.publication.link;
        delete $scope.publication.publication_title;
        delete $scope.publication.user;
        delete $scope.publication.verified;
        delete $scope.publication.__v;
        delete $scope.publication.versions;
        delete $scope.publication.date_added;
        delete $scope.publication.addedById;
        delete $scope.publication.flagEvents;
        delete $scope.publication.flagged;
        delete $scope.publication.flaggedReason;
    });

    $scope.openDeletePublicationModal = function(){
        $('#deletePublicationModal').openModal();
    }

    $scope.openVerifyPublicationModalMetadata = function(){
        $('#verifyPublicationModalMetadata').openModal();
    }

    $scope.deletePublication =  function(){
        Publications.deletePublication($scope.publicationId, $scope.deletionTitle, "No reason given").then(function (response){
            response = response.data;
            if(response.success == true){
                $('#deletePublicationModal').closeModal();
                Authentication.updateStats();
                $location.path('/');
            }
            Materialize.toast(response.message, 5000);
        });
    }

    $scope.verifyPublication = function (){
        Publications.verifyPublication($scope.publicationId).then(function (response){
            response = response.data;
            if(response.success == true){
                $location.path('/publication/' + $scope.publicationId);
                $('#verifyPublicationModalMetadata').closeModal();
                Authentication.updateStats();
            }
            Materialize.toast(response.message, 5000);
        });
    }

    $scope.openConfirmMetadataModal = function(key, value, prevValue){
        if(prevValue == value){
            Materialize.toast("No changes made", 3000);
        }
        else if(value == "" || value == undefined){
            Materialize.toast( "No value given", 3000);
        }
        else {
            $scope.tobeModifiedFeild = key;
            $scope.tobeModifiedValue = value;
            $scope.tobeModifiedPrevValue = prevValue;


            $('#confirmMetadataChange').openModal();
        }

    }

    $scope.saveMetadata = function (){
        if(($scope.tobeModifiedValue != "") && ($scope.tobeModifiedValue != undefined)){
            Publications.saveMetadata($scope.publicationId, $scope.tobeModifiedFeild, $scope.tobeModifiedValue).then(function (response){
                response = response.data;

                if(response.success == true){
                    Authentication.updateStats();
                    $scope.publication[$scope.tobeModifiedFeild] = $scope.tobeModifiedValue;
                    $scope['input_' + $scope.tobeModifiedFeild] = "";
                }

                $('#confirmMetadataChange').closeModal();
                Materialize.toast(response.message, 5000);
            });
        }
        else{
            Materialize.toast('Input field is empty. If you wish to simply delete this data, use the delete button', 5000);
        }
    }
    $scope.deleteMetadata = function (key){
        Publications.saveMetadata($scope.publicationId, key, '').then(function (response){
            response = response.data;

            if(response.success == true){
                $scope.publication[key] = "";
            }
            Materialize.toast(response.message, 5000);
        });
    }

    $(document).ready(function(){
        $('.collapsible').collapsible({
            accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $(".dropdown-button").dropdown();
    });

}]);
