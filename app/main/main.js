/**
 * Created by espenandreassen on 28.02.16.
 */

angular.module('app').controller('MainCtrl', ['$scope','$state', '$stateParams', '$location','Publications', '$sce', 'Authentication', function($scope,$state, $stateParams, $location,  Publications, $sce, Authentication){


    $scope.homeScreen = false; //Determines of info should be displayed
    $scope.loadingResults = true; //Hides/shows loading bar while waiting for results

    //Seting radio button as specified in url
    if ($stateParams.queryType != undefined){
        $scope.searchType = $stateParams.queryType;
    }
    else {
        $scope.searchType = "publication_title";
    }
    var altimetricInitiated = false;

    Authentication.updateStats();

    $scope.search = function (field, query){
        var altimetricInitiated = false;
        $scope.string = query;
        Publications.search(field, query)
            .success(function(recievedPublications) {
                $scope.string = query;
                $scope.publications = [];
                $scope.loadingResults = false;
                if(recievedPublications == 0){
                 }
                angular.forEach(recievedPublications, function(publication) {

                    if( publication.verified == true) {//Removing unverified publications
                        if(publication.doi != '') {
                            Publications.getCitationCount(publication.doi).then(function (response) {
                                response = response;
                                if(response.data["opensearch:totalResults"] != 0){
                                    publication.citationCount = response.data["search-results"].entry[0]["citedby-count"];
                                }
                                if(!altimetricInitiated){
                                    _altmetric_embed_init();
                                    altimetricInitiated = true;
                                }
                            });
                        }

                        //This is done to calculate number of metadata fields
                        publication.numberOfFields = -6;
                        angular.forEach(publication, function(publicationField) {
                            if (publicationField != ""){
                                publication.numberOfFields ++;
                            }
                        });

                        //Calculating which versions in each respective publication has the most votes, and thus the link to be used.
                        var topVersion = "";
                        for (var i = 0; i < publication.versions.length; i++) {
                            publication.versions[i].voteSum = (publication.versions[i].upvoting_users.length -
                            publication.versions[i].downvoting_users.length);

                            if ((topVersion == "") || (publication.versions[i].voteSum > topVersion.voteSum)) {
                                topVersion = publication.versions[i];
                            }
                        }
                        publication.topVersion = topVersion;

                        publication.publication_title = $sce.trustAsHtml(publication.publication_title) //So html in the lags in rendered
                        if((publication.abstract == undefined) || (publication.abstract == "")){
                            publication.abstract = "No abstract has been registered."
                        }
                        $scope.publications.push(publication);

                    }
                });
                $('.collapsible').collapsible({
                    accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                });
            });
    }
    $scope.search($scope.searchType, $stateParams.query);

    //Search when enter is pressed
    $scope.keyPress = function (keyEvent) {
        if (keyEvent.which === 13) {
            $state.go('search', {'query': $scope.string, 'queryType': $scope.searchType}, {reload: true});
        }
    };

    //If user search input is a doi, switch to doi search type
    $scope.change = function(){
        var pattern = /\b(10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+)\b/; //Matching doi (http://stackoverflow.com/questions/27910/finding-a-doi-in-a-document-or-page)
        if(pattern.test($scope.string) == true){
            if ($scope.searchType != "doi"){
                Materialize.toast("DOI detected", 3000);
            }
            $scope.searchType = "doi";
        }
    }


    //Used for disabling of buttons you need authorization to access
    $scope.user = Authentication.getCurrentUser();
    $scope.authorizationLevel = Authentication.getAccessLevel();
    if($scope.user == false){
        $scope.buttonText = "Sign in / register";
    }
    else{
        $scope.buttonText = "" + $scope.user.username + " - log out";
        $scope.picture = $scope.user.picture;
    }

    $scope.openLoginModal = function() {
        if($scope.user == false){
            $('#authenticationModal').openModal();
        }
        else{
            $('#logoutModal').openModal();
        }
    }

    $scope.goToPublication = function(_id) {
        $state.go('publication', {'publicationId': _id});
    }

    $scope.openInclusionGuidelines = function (){
        $rootScope.$broadcast("inclusionGuidelines");
    }

    $(document).ready(function() {
        $('.dropdown-button').dropdown({
                inDuration: 300,
                outDuration: 225,
                hover: true, // Activate on hover
                belowOrigin: true, // Displays dropdown below the button
                alignment: 'right' // Displays dropdown with edge aligned to the left of button
            }
        );
        $('select').material_select();
    });
}]);

//directive draving up a expandable abstract field
angular.module('app').directive('abstract', ['$sce', function($sce) {
    return {
        restrict: 'AE',
        replace: true,
        template: "<ul class='collapsible z-depth-0 no-border-sides' style='margin-bottom: 0px' data-collapsible='accordion'>" +
        "<li><div class='collapsible-header blue-grey-text' style='border-bottom-width: 0px' ng-click='changeIcon()'><i class='material-icons blue-grey-text'>{{icon}}</i>Expand abstract" +
        "</div>" +
        "<div class='collapsible-body expandable-body'><p class='no-padding' ng-bind-html='publication.abstract'/></div>" +
        "</li></ul>",

        link: function (scope, elem, attrs) {

            scope.htmlAbstract = scope.publication.abstract;
            scope.icon = "expand_more";
            //

            scope.changeIcon = function(){
                if( scope.icon == "expand_more"){
                    scope.icon = "expand_less"
                }
                else {
                    scope.icon = "expand_more";
                }
            }

            $('.collapsible').collapsible({
                accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
            })
        }
    }
}]);

