/**
 * Created by espenandreassen on 28.02.16.
 */

angular.module('app').controller('IndexCtrl', ['$scope', '$state', '$location','Publications', '$sce', 'Authentication', function($scope, $state, $location,  Publications, $sce, Authentication){

    $scope.homeScreen = true;
    $scope.eventLimit = 5;
    $scope.percentProgress = 40;
//test
    $scope.tobeModifiedFeild = "for";

    $scope.searchType = "publication_title";

    $scope.event = {'message': "", 'points': 0};
    Authentication.updateStats();

    // $pattern = '\b(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'<>])[[:graph:]])+)\b';
    // var patt = new RegExp($pattern);

    //If user search input is a doi, switch to doi search type
    $scope.change = function(){
        var pattern = /\b(10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+)\b/; //Matching doi (http://stackoverflow.com/questions/27910/finding-a-doi-in-a-document-or-page)
        if(pattern.test($scope.string) == true){
            if ($scope.searchType != "doi"){
                Materialize.toast("DOI detected", 3000);
            }
            $scope.searchType = "doi";
        }
    }

    if (Authentication.welcomeGiven() == false){
        $scope.newLevel = 1;
        $scope.newLevelUnlock = "Add papers and vote on the best version / source of a paper!";
        $('#levelUpModal').openModal();

        $scope.eventMessage = "Thank you for signing up, together we can make this service great! By contributing you will earn points, progress through levels, and gain awards and unlocks! Check out the progress board on the right side of the screen, and click the links there for more info!";
        $scope.eventTitle = "Welcome to OASearch!";
        $scope.eventPoints = "10";
        $('#eventModal').openModal();
    }

    //All controller can broadcast an event, which will trigger a modal with the provided data. Used for displaying awards/points.
    $scope.$on("event",function (event, data) {
        $scope.eventMessage = data.message;
        $scope.eventPoints = data.points;
        $scope.eventTitle = data.title;

        $('#eventModal').openModal();
    });
    $scope.$on("levelUp",function (event, data) {
        $scope.newLevel = data.level;
        $scope.newLevelUnlock = data.unlock;
        console.log("Level up");
        $('#levelUpModal').openModal();
    });

    $scope.openInclusionGuidelines = function (){
        $('#inclusionGuidelines').openModal();
    }

    $scope.$on("inclusionGuidelines", function (event, data){
        $('#inclusionGuidelines').openModal();
    });

    $scope.userStats = Authentication.User;


    $scope.search = function (){
        $state.go('search', {'query': $scope.string, 'queryType': $scope.searchType});
    }

    //Search when enter is pressed
    $scope.keyPress = function (keyEvent) {
        if (keyEvent.which === 13) {
            $scope.search();
        }
    };



    //Used for disabling of buttons you need authorization to access
    $scope.user = Authentication.getCurrentUser();
    $scope.authorizationLevel = Authentication.getAccessLevel();

    if($scope.user == false){
        $scope.buttonText = "Sign in / register";
    }
    else{
        $scope.buttonText = "" + $scope.user.username + " - log out";
        $scope.picture = $scope.user.picture;



    }

    $scope.openLoginModal = function() {
        if($scope.user == false){
            $('#authenticationModal').openModal();
        }
        else{
            $('#logoutModal').openModal();
        }
    }

    $scope.goToVerificationList = function(){
        if($scope.authorizationLevel < 1){
            Materialize.toast("You need to be signed in to verify papers", 4500);
        }

        else{
            $state.go('verification');
        }
    }

    $scope.goToFlagList = function (){
        $state.go('flags');
    }

    $scope.goToAddPublication = function(){
        if($scope.authorizationLevel < 1){
            Materialize.toast("You need to be signed in to add a paper.", 4500);
        }

        else{
            $state.go('add_publication');
        }
    }

    angular.element(document).ready(function () {
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $(".dropdown-button").dropdown();
        $(".flagDropdown").dropdown();
        $('ul.tabs').tabs();
        $('select').material_select();
    });

    $scope.publications = [];


    $(".button-collapse").sideNav();

    $scope.minRange = "0";
    $('.slider').slider({full_width: true});

    $scope.openContributionMenu = function() {
        $(".dropdown-button").dropdown();
    }


    $(document).ready(function() {
        $('select').material_select();
        $(".dropdown-button").dropdown();
    });

}]);


angular.module('app').directive('loginButton',['Authentication', function(Authentication) {
    return {
        restrict: 'AE',
        replace: true,
        template: "<a ng-click='openLoginModal()'>{{buttonText}} " +
        " <img ng-show='signedIn' height='45' width='45' style='vertical-align:middle; border-radius: 30%' ng-src={{user.picture}}>" +
        " <i ng-hide='signedIn' class='material-icons right'>account_box</i>" +
        "</a>",
        // <a ng-click='openLoginModal()' ><i class='material-icons right'>account_box</i>{{buttonText}}</a>
        link: function (scope, elem, attrs) {



            scope.user = Authentication.getCurrentUser();
            if(scope.user == false){
                scope.signedIn = false;
                scope.buttonText = "Sign in / register";
            }
            else if (scope.user.picture == 'account_box'){
                scope.buttonText = scope.user.username;
                scope.signedIn = false;
            }
            else{
                scope.buttonText = scope.user.username;
                scope.signedIn = true;
            }

            scope.openLoginModal = function() {
                if(scope.user == false){
                    $('#authenticationModal').openModal();
                }
                else{
                    $('#logoutModal').openModal();

                }
            }
        }
    }
}]);

angular.module('app').directive('loginModal', ['Authentication', '$window', function(Authentication, $window) {
    return {
        restrict: 'AE',
        replace: true,
        templateUrl: '/app/common/templates/loginModal.html',
        link: function (scope, elem, attrs) {

            scope.ongoingRegistrations = false;
            scope.keyPress = function (keyEvent) {
                if (keyEvent.which === 13) {
                    scope.signIn();
                }
            };


            $('ul.tabs').tabs();
            $('#myModal').leanModal({
                ready: function() { $('ul.tabs').tabs(); }
            });


            var validateRegistration = function(){
                if(scope.registerUsername == undefined || scope.registerPassword == undefined){
                    scope.logInValidationMessage = "please provide username and password to register";
                    return false;
                }
                else return true;
            }

            var validateLogIn = function() {
                if (scope.signInUsername == undefined || scope.signInPassword == undefined) {
                    scope.logInValidationMessage = "please provide username and password in log in field";
                    return false;
                }
                else return true;
            }

            scope.signIn = function(){
                if(validateLogIn()){
                    Authentication.login(scope.signInUsername, scope.signInPassword).then(function(response) {
                        response = response.data
                        if(response.success == true){
                            Authentication.putUserObject(response.user);
                            Authentication.setCurrentUser(response.user.username, "account_box");
                            Authentication.setAccessLevel('1');
                            //   $route.reload()
                            $window.location.reload();
                            $('#authenticationModal').closeModal();
                        }
                        if(response.success == false){
                            scope.logInValidationMessage = response.message;
                        }
                    });
                }
            }

            scope.register = function(){
                if(validateRegistration()){
                    scope.ongoingRegistrations = true;
                    Authentication.register(scope.registerUsername, scope.registerPassword).then(function(response) {
                        response = response.data
                        console.log(response.user);
                        if(response.success == true){
                            Authentication.login(scope.registerUsername, scope.registerPassword).then(function(response) {
                                response = response.data
                                if(response.success == true){
                                    Authentication.setCurrentUser(response.user.username);
                                    Authentication.setAccessLevel('1');
                                    Authentication.newEventCookieRegistered();
                                    $window.location.reload();
                                }
                                if(response.success == false){
                                    scope.ongoingRegistrations = false;
                                    Materialize.toast("New user created, but we were unable to sign you in for some reason. Please try to refresh and sign in with your new username.", 10);
                                    scope.logInValidationMessage = "Err: New account created, but not signed in";
                                }
                            });
                        }
                        else{
                            scope.ongoingRegistrations = false;
                            scope.logInValidationMessage = response.message;
                        }
                    });
                }
            }

            scope.logout = function(){
                Authentication.logout();
                $window.location.reload();
            }

        }
    }
}]);
