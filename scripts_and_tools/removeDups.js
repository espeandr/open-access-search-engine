var duplicates = [];

db.publications.aggregate([
        { $group: {
            _id: { publication_title: "$publication_title"}, // can be grouped on multiple properties
            dups: { "$addToSet": "$_id" },
            count: { "$sum": 1 }
        }},
        { $match: {
            count: { "$gt": 1 }    // Duplicates considered as count greater than one
        }}
    ], {
        allowDiskUse:true,
    })               // You can display result until this and check duplicates
    // If your result getting response in "result" then use else don't use ".result" in query
    .forEach(function(doc) {
        doc.dups.shift();      // First element skipped for deleting
        doc.dups.forEach( function(dupId){
                duplicates.push(dupId);   // Getting all duplicate ids
            }
        )
    })

// If you want to Check all "_id" which you are deleting else print statement not needed

printjson("Duplicates found: " + duplicates.length);
printjson("Publications before cleaning" + db.publications.count());

// Remove all duplicates in one go    
db.publications.remove({_id:{$in:duplicates}})
printjson("Publications after cleaning" + db.publications.count());
