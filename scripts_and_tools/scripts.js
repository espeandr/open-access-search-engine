var isProduction = true; //Set this to true if running on production environment.

var port;
var database;
var callbackUrl;
var request = require('request'); //Used for making http requests to external dbs


var productionPort = 80;
var productionDatabase = 'mongodb://localhost:27017/local';
var productionFacebookCallbackUrl = 'http://www.oasearch.com/auth/facebook/callback';

var localPort = 8080;
var localDatabase = 'mongodb://localhost:27017/testDB'; //Configure to connect to DB
var localFacebookCallbackUrl = 'localhost:8080/auth/facebook/callback';

isProduction ? (port = productionPort, database = productionDatabase, callbackUrl = productionFacebookCallbackUrl) :
    (port = localPort, database = localDatabase, callbackUrl = localFacebookCallbackUrl);
//----------------------------------------------------------------------------------------------------------------------
//Dependencies
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var mongosession = mongoose.connect(database);     // connect to mongoDB database on modulus.io
var Account = require('../models/account');
var Publication = require('../models/publication');
var FlagEvent = require('../models/flagEvent');
var Version = require('../models/version');

var args = process.argv.slice(2);

var removePublicationsWithoutLinks = function(){
    var missingLink = 0;
    var errorHeader = 0;
    var toBeDeleted = 0;
    var checked = 0;
    var checkedPrintCounter = 0;

    console.log("running ")

    Publication.count({}, function(err, count){
        console.log( "Number of publications: ", count );


        var stream = Publication.find().stream();

        stream.on('data', function (doc) {
            this.pause()

            checked ++;
            checkedPrintCounter ++;
            if((Math.floor(checkedPrintCounter / count * 100) >= 1)){
                checkedPrintCounter = 0;
                console.log(Math.floor((checked / count * 100)) + "% complete");
                console.log( missingLink + " deleted so far because of missing link")
                console.log("--------------")

            }
            if(doc.versions[0].link != ''){
                this.resume()
            }
            else{
                Publication.findByIdAndRemove(doc._id, function (err, success) {
                    if (err) {
                        console.log("error removing empty string");
                    }
                    else {
                        missingLink ++;
                    }
                })

                this.resume()
            }

        })

        stream.on('error', function (err) {
            console.log("error: " + err)
            // handle err
        })

        stream.on('close', function () {
            console.log("all done")
            // all done
        })
    });
}



var removePublicationsWithBrokenLink = function(){


    //100 - 2000 = 153 sek : 21 deleted , 19 errors
    //150 - 2000 = 149 sek: 21 deleted , 19 errors

    var threads = 50;
    var limit = 150000;

    var baseRequest = request.defaults({
        'timeout': 5000
    })

    var linksChecked = 0;
    var activeThreads = threads;
    var completedThreads = 0;

    var completeKeeper = function(thread){
        completedThreads ++;
        if(linksChecked == total){
            console.log("======================== FINISHED ======================")
            console.log("========================================================")
            console.log("========================================================")
            console.log("========================================================")
            console.log("=== All " + threads + " threads are done ===")
            console.log("Total time deleting all relevant publications: ");
            console.timeEnd("deletingProcess");
            console.log("======================== FINISHED ======================")
            console.log("========================================================")
            console.log("========================================================")
            console.log("========================================================")
        }
    }

    var linkErrors = 0;

    var errorHeader = 0;
    var checked = 0;
    var checkedPrintCounter = 0;
    var errors = 0;
    console.log("running ")

    var total = 0;
    Publication.count({}, function(err, count) {
        console.log("Number of publications: ", count);

        total = count;
        //   total = limit
        var testURL = ""
        var stream = Publication.find().stream();



        var pubLinks = [];
        var deleted = 0;

//---------------
        var deleteInArray = function(thread, index, max){
            var start = new Date().getTime();
            if(index < (max)){
                linksChecked ++;

                /*
                 //  console.log("active: " + activeThreads + " threads" + threads);
                 if(activeThreads < threads){
                 deleteInArray(threads + completedThreads, (Math.floor((max - index)/2)) + index, max);
                 max = Math.floor(((max - index) / 2) + index);
                 console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                 console.log("splitting thread: " + thread);
                 console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                 activeThreads ++;
                 }
                 */

                baseRequest(pubLinks[index].link, function (error, response, body) {
                    if (error) {
                        errors ++ ;
                        linkErrors++
                        index ++;
                        deleteInArray(thread, index, max);
                    }
                    else {
                        if (response.statusCode == 404){
                            Publication.findByIdAndRemove(pubLinks[index]._id, function (err, found) {
                                if (err) {
                                    console.log("error removing empty string");
                                    index ++;
                                    deleteInArray(thread, index, max);
                                }
                                else {
                                    deleted ++;
                                    index ++;
                                    deleteInArray(thread, index, max);
                                }
                            })
                        }
                        else {
                            index ++;
                            deleteInArray(thread,index, max);
                        }
                    }
                })
            }
            else {
                console.log("=======================")
                console.log("thread " + thread +" is done checking publications " + thread * (total / threads) + " to " + max);
                console.log("=======================");
            }
        }
        //----------------


        console.log("PART 1 / 2 -- Extracting links to check");

        stream.on('data', function (doc) {
            this.pause()

            checked ++;
            checkedPrintCounter ++;
            if((Math.floor(checkedPrintCounter / count * 100) >= 1)){
                checkedPrintCounter = 0;
                console.log(Math.floor((checked / count * 100)) + "% complete");
                console.log( checked + " links registered so far")
                console.log("--------------")
            }

            var publication = {
                link: doc.versions[0].link,
                _id: doc._id
            }
            pubLinks.push(publication);
            this.resume()


        })

        stream.on('error', function (err) {
            console.log("error: " + err)
            // handle err
        })

        stream.on('close', function () {
            var tmpChecked = 0
            console.time("deletingProcess");
            for (i = 0; i < (threads ); i ++){
                deleteInArray(i, Math.floor( i * total / threads), Math.floor( i * total / threads + (total /threads)))
            }
            var timer = setInterval(function() {

                console.log(linksChecked / total * 100 + "% of the publications has been checked for 404s");
                console.log(deleted + " deleted");
                console.log(linkErrors + " responded with an error, and was not deleted");
                console.log(linksChecked + " publication links have been checked");
                console.log(completedThreads + " / " + threads + " threads completed");
                console.log(((linksChecked - tmpChecked)/8) + " checked/second");
                console.log("#########################################################");
                tmpChecked = linksChecked;

            }, 8000)
        })
    })
}

var commands = ["removePublicationsWithoutLink, removePublicationsWithBrokenLink"]

switch (args[0]) {
    case "removePublicationsWithoutLink":
        console.log("Removing publications without link");
        removePublicationsWithoutLinks();
        break;
    case "removePublicationsWithBrokenLink":
        console.log("Removing publications with broken link.");
        removePublicationsWithBrokenLink();
        break;
    default:
        console.log("Command not found. Available commands:");
        for (i = 0; i < commands.length; i ++){
            console.log(commands[i]);
        }
        process.exit()
}