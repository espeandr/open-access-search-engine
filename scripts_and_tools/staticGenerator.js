//NOTE!!!!!
//PhantomJS must be installed for this to run.
//Install command: sudo npm install -g phantomjs-prebuilt
//Run command: phantomjs staticGenerator.js;

var outputFileName = "testing.html"
var URLtoExport = "http://localhost:8080/#/search?queryType=publication_title&query=a%20test%20query";
var waitingTime = 10000; // Change timeout as required to allow sufficient time



var page = new WebPage()
var fs = require('fs');

page.open(URLtoExport, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit();
    } else {
        window.setTimeout(function () {
            page.evaluate(function() {
            });
            fs.write(outputFileName, page.content, 'w');
            console.log("done");
            phantom.exit();
        }, waitingTime);
    }
});
