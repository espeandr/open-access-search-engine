

**This is the source code of OAsearch, available at [oasearch.com](http://www.oasearch.com)
The application is built using the MEAN stack: MongoDB, ExpressJS, AnuglarJS and NodeJS.
**
## How do I set it up?

### Requierements ###
 - MongoDB
 - NodeJS
 - Bower

### Steps ###
 - Clone repository
 - Run bower install
 - Run npm install
 - Configure database connection in server.js (top of the file)
 - Start node server.js

### License ###
MIT, see the license file.